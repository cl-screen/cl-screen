(in-package :cl-screen)

(define-foreign-library libslang
    (:unix (:or  "libslang.so.1" "libslang.so"))
  (t (:default "libslang")))

(use-foreign-library libslang)


;;; SLtt functions

(defcfun ("SLtt_get_terminfo" sltt-get-terminfo) :void)
(defcvar ("SLtt_Use_Ansi_Colors" sltt-use-ansi-colors) :int)
(defcvar ("SLtt_Screen_Rows" sltt-screen-rows) :int)
(defcvar ("SLtt_Screen_Cols" sltt-screen-cols) :int)
(defcfun ("SLtt_set_color" sltt-set-color) :void (obj :int) (name :string) (fg :string) (bg :string))
(defcfun ("SLtt_get_screen_size" sltt-get-screen-size) :void)
(defcfun ("SLtt_tgetstr" sltt-tgetstr) :string (char :string))
(defcfun ("SLtt_beep" sltt-beep) :void)
(defcfun ("SLtt_set_cursor_visibility" sltt-set-cursor-visibility) :int (flag :int))

;;; SLkp functions

(defcfun ("SLkp_init" slkp-init) :int)
(defcfun ("SLkp_define_keysym" slkp-define-keysym) :int (key :string) (sym :unsigned-int))
(defcfun ("SLkp_getkey" slkp-getkey) :int)

;;; SLang functions

(defcfun ("SLang_init_tty" slang-init-tty) :void (abort-char-code :int) (flow-control :int) (output-processing :int))
(defcfun ("SLang_input_pending" slang-input-pending) :int (time :int))
(defcfun ("SLang_reset_tty" slang-reset-tty) :void)
(defcfun ("SLang_getkey" slang-getkey) :uint)
(defcfun ("SLang_process_keystring" slang-process-keystring) :string (keystring :string))
(defcfun ("SLang_ungetkey" slang-unget-key) :void (ch :uchar))
(defcfun ("SLang_flush_input" slang-flush-input) :void)
(defcvar ("SLKeyBoard_Quit" sl-keyboard-quit) :int)

;;; SLsmg functions

(defcfun ("SLsmg_init_smg" slsmg-init-smg) :int)
(defcfun ("SLsmg_cls" slsmg-cls) :void)
(defcfun ("SLsmg_gotorc" slsmg-gotorc) :void (row :int) (col :int))
(defcfun ("SLsmg_write_char" slsmg-write-char) :void (ch :char))
(defcfun ("SLsmg_write_string" slsmg-write-string) :void (string :string))
(defcfun ("SLsmg_refresh" slsmg-refresh) :void)
(defcfun ("SLsmg_reset_smg" slsmg-reset-smg) :void)
(defcfun ("SLsmg_draw_hline" slsmg-draw-hline) :void (len :unsigned-int))
(defcfun ("SLsmg_draw_vline" slsmg-draw-vline) :void (len :int))
(defcfun ("SLsmg_set_char_set" slsmg-set-char-set) :void (set :int))
(defcfun ("SLsmg_erase_eol" slsmg-erase-eol) :void)
(defcfun ("SLsmg_erase_eos" slsmg-erase-eos) :void)
(defcfun ("SLsmg_set_color" slsmg-set-color) :void (obj :int))
(defcfun ("SLsmg_reinit_smg" slsmg-reinit-smg) :void)
(defcfun ("SLsmg_get_column" slsmg-get-column) :int)
(defcfun ("SLsmg_get_row" slsmg-get-row) :int)