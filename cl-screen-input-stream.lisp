(in-package :cl-screen)

(defun valid-key-p (key-code)
    (< key-code 255))

(defun wait-key ()
  (when (zerop (slang-input-pending 0))
	(wait-key)))

(defun get-key ()
  (wait-key)
  (let ((key-code (slang-getkey)))
	(if (valid-key-p key-code)
		key-code
		(get-key))))

(defclass cl-screen-input-stream
    (fundamental-character-input-stream
     trivial-gray-stream-mixin)
  ((screen :initform nil 
	   :initarg :screen 
	   :accessor screen-of 
	   :documentation "Screen that this stream interacts with" )
   (prompt :initform ">"
	   :initarg :prompt
	   :accessor prompt-of
	   :documentation "Prompt for input")
   (row    :initform 0
	   :accessor row-of
	   :initarg :screen-row
	   :documentation "Row on screen at which input appears")
   (width  :initform 0
	   :accessor width-of
	   :documentation "Width allowed for input")
   (previous-char :initform #\Space
		  :accessor previous-char-of
		  :type base-char
		  :documentation "Character previously typed by user")
   (current-char :initform #\Space
		 :accessor current-char-of
		 :type base-char
		 :documentation "Character currently typed by user"))
   (:documentation "Window class that can be treated as an input stream with a configurable prompt."))


(defmethod initialize-instance :after ((s cl-screen-input-stream) &rest args)
  (declare (ignore args))
  (setf (values (row-of s) (width-of s)) (get-screen-size (screen-of s)))
  (decf (row-of s)))

(defmethod stream-read-char ((s cl-screen-input-stream) )
  (let 
      ((result (code-char (slang-getkey))))
    (setf (previous-char-of s) (current-char-of s))
    (setf (current-char-of s) result)))

(defmethod stream-peek-char ((s cl-screen-input-stream))
  (let ((key-code (slang-getkey)))
	(slang-unget-key key-code)
	(code-char key-code)))
	  
(defmethod stream-unread-char ((s cl-screen-input-stream) (c character))
  (slang-unget-key (char-code c))   
  (setf (current-char-of s) c))

(defmethod stream-read-char-no-hang ((s cl-screen-input-stream))
  (when (> (slang-input-pending 0) 0)
    (stream-read-char s)))

(defmethod stream-clear-input ((s cl-screen-input-stream))
  (set-cursor (screen-of s) (row-of s) 0)
  (loop
	 for i from 0 below (width-of s)
	 do
       (write-string-at-cursor (screen-of s) (string #\Space)))
  (finish-screen (screen-of s))
  (slang-flush-input))

;; listen
(defmethod stream-listen ((s cl-screen-input-stream))
  (not (zerop (slang-input-pending 0))))

;; stream read line
(defmethod stream-read-line ((s cl-screen-input-stream))
  (set-cursor (screen-of s) (row-of s) 0)
  (erase-from-cursor-to-eol (screen-of s))
  (write-string-at-cursor (screen-of s) 
                          (concatenate 'string (prompt-of s) "> "))
  (sltt-set-cursor-visibility 1)
  (finish-screen (screen-of s))
  (let ((line 
	 (make-array (- (width-of s) (length (prompt-of s))) 
		     :element-type 'character
		     :fill-pointer 0 :adjustable t)))
    (loop 
       for chr = (stream-read-char s)
       until (char= chr #\Return)
       do
         (progn
           (format t "~X " (char-code chr))
           (cond
             ((graphic-char-p chr)
              (progn
                (write-string-at-cursor (screen-of s) (string chr))
                (finish-screen (screen-of s))
                (vector-push-extend chr line)))
             ((or
               (char= #\Backspace chr)
               (= (char-code chr) #X7F))
              (when (not (zerop (fill-pointer line)))
                (setf (fill-pointer line) (1- (fill-pointer line)))
                (cursor-back (screen-of s))
                (write-string-at-cursor (screen-of s) " ")
                (cursor-back (screen-of s))
                (finish-screen (screen-of s)))))))
    (set-cursor (screen-of s) (row-of s) 0)
    (erase-from-cursor-to-eol (screen-of s))
    (sltt-set-cursor-visibility 0)
    (finish-screen (screen-of s))
    (values line nil)))

	 