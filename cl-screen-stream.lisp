
(in-package :cl-screen)

;;
;; Stream used to write to a slang screen. If you don't provide a
;; screen on initialization, it will create one. Uses
;; trivial-gray-streams
;;
(defclass cl-screen-stream 
    (fundamental-character-output-stream
     trivial-gray-stream-mixin)
  ((screen :initform nil 
		   :initarg :screen 
		   :accessor screen-of 
		   :documentation "Screen that this stream interacts with" )
   (background-color
	:initform 'black
	:accessor background-color-of
	:documentation "Foreground colour currently being used for characters writen to the stream")
   (foreground-color
	:initform 'white
	:accessor foreground-color-of
	:documentation "Foreground colour currently being used for chararcters written to the stream")
   (current-char 
	:type character 
	:initform nil 
	:accessor current-char-of
	:documentation "Char currently read from this stream")
   (previous-char 
	:type character 
	:initform nil 
	:accessor previous-char-of
	:documentation "Char previously read from this stream")))


;;
;; Standard methods used to initialise a stream
;;

(defmethod initialize-instance :after ((s cl-screen-stream) &rest args)
  (declare (ignore args))
    (when (null (screen-of s))
      (setf (screen-of s) (make-instance 'tty-screen))
      (initialize-screen (screen-of s))))

(defmethod stream-read-char ((s cl-screen-stream))
  (let ((result (slang-getkey)))
    (setf (previous-char-of s) (current-char-of s))
    (setf (current-char-of s) result)))

(defmethod stream-unread-char ((s cl-screen-stream) (c character))
  (slang-unget-key c)
  (setf (current-char-of s) c))

(defmethod stream-read-char-no-hang ((s cl-screen-stream))
  (when (> (slang-input-pending 0) 0)
    (stream-read-char s)))

(defmethod stream-clear-input ((s cl-screen-stream))
  (slang-flush-input))

(defmethod stream-write-char ((s cl-screen-stream) (c character))
  (set-color (screen-of s) (foreground-color-of s) (background-color-of s))
  (slsmg-write-char (char-code c)))

(defmethod stream-line-column ((s cl-screen-stream))
 (slsmg-get-column))

(defmethod stream-start-line-p ((s cl-screen-stream))
  (= 0 (slsmg-get-column)))
    
(defmethod stream-terpri ((s cl-screen-stream))
  (slsmg-gotorc (1+ (slsmg-get-row)) 0))

(defmethod stream-advance-to-column ((s cl-screen-stream) column)
  (slsmg-gotorc (slsmg-get-row) column))

(defmethod stream-clear-output ((s cl-screen-stream))
  (clear-screen (screen-of s)))

(defmethod stream-finish-output ((s cl-screen-stream))
  (finish-screen (screen-of s)))

(defmethod stream-force-output ((s cl-screen-stream))
  (finish-screen (screen-of s)))

(defmethod close ((s cl-screen-stream) &key abort)
  (declare (ignore abort))
  (release-screen (screen-of s))
  (setf (screen-of s) nil))

(defmethod open-stream-p ((s cl-screen-stream))
  (not (null (screen-of s))))

