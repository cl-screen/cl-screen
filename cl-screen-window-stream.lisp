
;;
;; A stream that writes to a window on a cl-screen stream, and has a
;; scrollback / history buffer, as well as colour attributes. No cursor
;; positioning within the window is allowed other than setting the column.
;; 

(in-package :cl-screen)

;;
;; A window has a sequence of logical lines which are lines of text
;; written to the window via the stream the window retains a complete
;; history.
;;
(defclass history-line ()
  ((line-cursor
    :initform 0
    :accessor cursor-of
    :documentation "The position the next character will be printed at in this line")
   (line-width
    :initarg :width
    :reader width-of)
   (line-text 
    :initform nil 
    :accessor text-of 
    :documentation "A line of text in a window history"))
  (:documentation "An entry in a history buffer of text lines sent to a window"))

(defmethod initialize-instance :after ((self history-line) &rest args)
  (declare (ignore args))
  (setf (text-of self) 
	(make-array (width-of self) :element-type 'character :adjustable nil :initial-element #\Space)))

(defgeneric add-space-to-history-line (line))

(defgeneric add-char-to-history-line (line chr))

(defgeneric remove-char-from-history-line (line))

(defgeneric print-history-line-to-window (line window row))

(defmethod add-space-to-history-line ((line history-line))
  "(add-space-to-history-line line)
Pad a logical line with a space on the end"
  (when (< (cursor-of line) (length (text-of line)))
    (setf (aref (text-of line) (cursor-of line)) #\Space)
    (incf (cursor-of line))))

(defmethod add-char-to-history-line ((line history-line) chr)
  "(add-char-to-history-line line chr column) 
Add a character to a logical line, positioned at the given column index."
  (assert (typep chr 'character))
    ;; change final space to our emitted char
  (when (< (cursor-of line) (length (text-of line)))
    (setf (aref (text-of line) (cursor-of line)) chr)
    (incf (cursor-of line))))

(defun trim-last-char (str)
  (subseq str 0 (1- (length str))))

(defmethod remove-char-from-history-line ((line history-line))
  "(remove-char-from-history-line chr column) 
Remove the character from the line at column from history"
  (when (not (zerop (length (text-of line))))
    (setf (text-of line) (trim-last-char (text-of line)))
    (decf (cursor-of line))))


(defmethod print-history-line-to-window ((line history-line) 
					 (window cl-screen-window) row)
  "(print-history-line-to-window history-line cl-screen-window) 
Print max-length chars of a given logical line on a cl-screen window starting from the given column"
  (position-window-cursor window 0 row)
  (let ((line-length
	 (min (length (text-of line)) (width-of window))))
    (flet ((nth-char-in-line (nth line)
	     (char (text-of line) nth)))
      (loop
	 :for i from 0 below line-length
	 :do
	 (add-char-to-window window (nth-char-in-line i line))))))

;;
;; The actual stream for writing to a window 
;;
(defclass cl-screen-window-stream 
	(cl-screen-window cl-screen-stream)
  ((lines :initarg nil :accessor lines-of
	  :documentation "Stream that prints to a history buffer for window")))


(defmethod initialize-instance :after 
	((self cl-screen-window-stream) &rest args)
  (declare (ignore args))
  ;; create a vector of logical lines
  (setf (lines-of self) 
	(make-array 0 :element-type 'history-line  :adjustable t :fill-pointer 0))
  ;; push first logical line into vector
  (vector-push-extend (make-instance 'history-line :width (width-of self)) (lines-of self)))
 

(defgeneric last-line-of (strm))

(defgeneric add-char (strm chr))

(defgeneric add-line (strm))

(defgeneric refresh-window (strm &key position-cursor))

(defmethod last-line-of ((s cl-screen-window-stream))
  "(last-line-of cl-screen-window-stream)
Return the last line in the array of logical lines (the one we write to)"
  (aref (lines-of s) (1- (length (lines-of s)))))

(defmethod add-char ((s cl-screen-window-stream) chr)
  "(add-char cl-screen-window-stream chr)
Add a sigle character to the stream window"
  (assert (typep chr 'character))
  (cond 
    ((graphic-char-p chr)
     (progn
       (add-char-to-history-line 
	(last-line-of s)
	chr)))
    ((char= #\Backspace)
     (remove-char-from-history-line
      (last-line-of s)))))
      
;;
;; Adds a new history line to the lines associated with a window
;;
(defmethod add-line ((s cl-screen-window-stream))
  "(add-line cl-screen-window-stream)
Add a new line to the history buffer stream"
  (vector-push-extend (make-instance 'history-line :width (width-of s)) (lines-of s)))

;;
;; standard stream - defining methods
;;
(defmethod stream-write-char ((s cl-screen-window-stream) (c character))
  "(stream-write-char cl-screen-window-stream character)
Stream based write char to window method"
  (cond 
    ;; if we write a new line, bump down the cursor 
    ((char= c #\Newline)
     (add-line s))
    ;; if we write a tab, bump on x modulo 4
    ((char= c #\Tab)
     (let 
	 ((new-cursor-x (mod (+ (cursor-of (last-line-of s)) 4) 4)))
       (when (< new-cursor-x (length (text-of (last-line-of s))))
	 (setf (cursor-of (last-line-of s)) new-cursor-x))))
    ;; carriage return sets the first column to leftmost
    ((char= c #\Return)
     (setf (cursor-of (last-line-of s)) 0))
    ;; otherwise just show the char in place
    (t
     (add-char s c))))
	  

(defmethod stream-line-column  ((s cl-screen-window-stream))
  "(stream-line-column cl-screen-window-stream)
Return the column # of the cursor in the stream window"
  (cursor-of (last-line-of s)))

(defmethod stream-start-line-p ((s cl-screen-window-stream))
"(stream-start-line-p cl-screen-window-stream)
  Return t if the cursor is at the lhs of the window"
  (= 0 (cursor-of (last-line-of s))))

(defmethod stream-terpri ((s cl-screen-window-stream))
  "Print a newline in the window"
  (add-line s))

(defmethod stream-advance-to-column ((s cl-screen-window-stream) column)
  "Advance the cursor to the given column in the window"
  (setf (cursor-of (last-line-of s)) (min column (width-of s))))


(defmethod stream-clear-output ((s cl-screen-window-stream))
  "(stream-clear-output cl-screen-window-stream))
Clears the history buffer completely"
  ;; create a vector of logical lines
  (setf (lines-of s) 
	(make-array 0 :element-type 'history-line  :adjustable t :fill-pointer 0))
  ;; push first logical line into vector
  (vector-push-extend (make-instance 'history-line :width (width-of s)) (lines-of s)))

(defmethod stream-finish-output ((strm cl-screen-window-stream))
  "(stream-finish-output cl-screen-window-stream) 
Write the stored logical lines that are visible in the stream window to the screen"
  (loop
     for y from (1- (height-of strm)) downto 0
     for index from (1- (length (lines-of strm))) downto 0
     do
       (print-history-line-to-window
	(the history-line
	  (aref (lines-of strm) index))
	strm
	y)))

(defmethod stream-force-output ((s cl-screen-window-stream))
  "(stream-finish-output cl-screen-window-stream) Write the stored
logical lines that are visible in the stream window to the screen"
  (stream-finish-output s))

(defmethod refresh-window ((s cl-screen-window-stream) &key position-cursor)
  "(refresh-window cl-screen-window :position-cursor p) Send the
history lines to the window, then write the text of the window to
the main screen. If p is non nil position the screen cursor at
the window cursors position."
  (declare (ignore position-cursor))
  (stream-force-output s)
  (call-next-method))
