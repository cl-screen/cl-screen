
(in-package :cl-screen)

(defclass cl-screen-window 
    ()
  ((screen :initarg :screen 
	   :accessor screen-of 
	   :documentation "Screen that this window interacts with" )
   (left-x :initform 0 
	   :initarg :left-x 
	   :accessor left-x-of 
	   :documentation "Leftmost column index of window on screen")
   (right-x :initarg :right-x 
	    :accessor right-x-of 
	    :documentation "Rightmost column index of window on screen")
   (top-y :initform 0 
	  :initarg :top-y 
	  :accessor top-y-of 
	  :documentation "Topmost row index of window on screen" )
   (bottom-y :initarg :bottom-y 
	     :accessor bottom-y-of 
	     :documentation "Bottom row index of window on screen")
   (cursor-x :initform 0 
	     :accessor cursor-x-of
	     :documentation "Position of cursor relative to left of window")
   (cursor-y :initform 0 
	     :accessor cursor-y-of
	     :documentation "Position of cursor relative to top of window")
   (foreground-color :initform 'white
		     :accessor foreground-of
		     :initarg :background
		     :documentation "Foreground colour of window")
   (background-color :initform 'black
		     :initarg :background
		     :accessor background-of
		     :documentation "Background color of window")
   (text :initform nil
	 :accessor text-of
	 :documentation "Text in window as an array of strings."))
  (:documentation "A rectangluar region of a cl-screen with its own local text, colours and cursor."))
	

	
(defmethod initialize-instance :after ((window cl-screen-window) &rest args)
  (declare (ignore args))
  (setf (text-of window) (make-array (list 
				      (width-of window) 
				      (height-of window)) 
				     :initial-element #\Space
				     :element-type 'base-char)))

(defgeneric height-of (window))

(defgeneric width-of (window))

(defgeneric refresh-window (window &key position-cursor))

(defgeneric clear-line-in-window (window))

(defgeneric add-char-to-window (window chr))

(defgeneric add-string-to-window (window str))

(defmethod height-of ((window cl-screen-window))
  "(height-of cl-screen-window)
Returns the height of the given window"
 (1+ (-  (bottom-y-of window) (top-y-of window))))

(defmethod width-of ((window cl-screen-window))
  "(width-of cl-screen-window)
Returns the width of the given window"
 (1+  (- (right-x-of window) (left-x-of window))))

(defmethod at-screen-rc ((window cl-screen-window) ix iy)
  "Translate a pair of window indices to screen indices"
  (values (+ (left-x-of window) ix) (+ (top-y-of window) iy)))

(defmethod goto-screen-rc ((window cl-screen-window) ix iy)
  "Translate a pair of window indices to screen indices and set the cursor there."
  (slsmg-gotorc  (+ (top-y-of window) iy) (+ (left-x-of window) ix)))

(defmethod refresh-window ((window cl-screen-window) &key position-cursor)
  "(refresh-window cl-screen-window :position-cursor p)
Write the text of the window to the main screen. If p is non nil position the screen 
cursor at the window cursors position."
  (progn
    (set-color (screen-of window) 
	       (foreground-of window) (background-of window))
    (loop
       for y from 0 below (height-of window)
       do
	 (loop
	    for x from 0 below (width-of window)
	    do
	      (progn
		(goto-screen-rc window x y)
		(slsmg-write-char 
		 (char-code (aref (text-of window) x y))))))
      (if position-cursor 
	  (goto-screen-rc 
	   window
	   (cursor-x-of window) 
	   (cursor-y-of window))))

(defmethod clear-line-in-window ((window cl-screen-window))
  "(clear-line-in-window cl-screen-window)
Clear the current line of the window"
  (loop
     for x from 0 below (width-of window)
     do 
       (setf (aref (text-of window) x (cursor-y-of window)) #\Space))))

(defmethod add-char-to-window ((window cl-screen-window) chr)
  "(add-char-to-window window chr) 
Place chr at the cursor position in the window and advance the cursor position accordingly"
  (with-accessors 
	((text text-of)
	 (cursor-x cursor-x-of)
	 (cursor-y cursor-y-of)) window
    (when (and (< cursor-x (width-of window))
	       (< cursor-y (height-of window))
	       (>= cursor-x 0)
	       (>= cursor-y 0))
      (cond
	;; if we write a new line, bump down the cursor 
	((char= chr #\Newline)
	 (incf cursor-y)
	 (setf cursor-x 0))
	;; if we write a tab, bump on x modulo 4
	((char= chr #\Tab)
	 (let 
	     ((new-cursor-x (mod (+ cursor-x 4) 4)))
	   (if (< new-cursor-x (width-of window))
	       (setf cursor-x new-cursor-x))))
	;; carriage return sets the first column to leftmost
	((char= chr #\Return)
	 (setf cursor-x 0))
	;; just bung the char in
	(t
	 (setf (aref text cursor-x cursor-y) chr)
	 (incf cursor-x))))))



(defmethod add-string-to-window ((window cl-screen-window) str)
  "(add-string-to-window window str) 
Places the characters in str onto the window by repeated calls to add-char-to-window"
  (loop
     for chr across str
     do
     (add-char-to-window window chr)))

(defgeneric position-window-cursor (window  column row))

(defmethod position-window-cursor ((window cl-screen-window) column row)
  "(position-window-cursor ((window cl-screen-window) column row))
Sets the windows cursor to be at the colum, row offset from its top left hand corner"
  (if (and (>= column 0) (< column (width-of window)))
      (setf (cursor-x-of window) column))
  (if (and (>= row 0) (< row (height-of window)))
      (setf (cursor-y-of window) row)))