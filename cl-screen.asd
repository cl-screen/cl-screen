;;;; Silly emacs, this is -*- Lisp -*-

(in-package :asdf)

(defsystem :cl-screen
    :name "CL-SCREEN"
    :author "Brian Mastenbrook"
    :version "1.0"
    :licence "MIT"
    :depends-on (:cffi :trivial-gray-streams)
    :description "Console library for Common Lisp"
    :components ((:file "package")
		 (:file "parse-float" :depends-on ("package"))
                 (:file "api" :depends-on ("package"))
                 (:file "aliens" :depends-on ("package"))
                 (:file "cl-screen" :depends-on ("package" "api" "aliens"))
				 (:file "cl-screen-window" :depends-on 
						("package" "api" "aliens" "cl-screen"))
				 (:file "cl-screen-stream" :depends-on 
						("package" "api" "aliens" "cl-screen"))
				 (:file "cl-screen-input-stream" :depends-on 
						("package" "api" "aliens" 
							   "cl-screen" 
							   "cl-screen-stream"))
				 (:file "cl-screen-window-stream" :depends-on 
						("package" "api" "aliens" 
							   "cl-screen" 
							   "cl-screen-window" 
							   "cl-screen-stream"))))
