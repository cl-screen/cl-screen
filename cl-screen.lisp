
(in-package :cl-screen)

(defclass cl-screen () ())


(defvar *initialized* nil)

(defvar *screen-rows* 0)
(defvar *screen-cols* 0)

(defvar *SL_KEY_UP*	        #X101)
(defvar *SL_KEY_DOWN*		#X102)
(defvar *SL_KEY_LEFT*		#X103)
(defvar *SL_KEY_RIGHT*		#X104)
(defvar *SL_KEY_PPAGE*		#X105)
(defvar *SL_KEY_NPAGE*		#X106)
(defvar *SL_KEY_HOME*		#X107)
(defvar *SL_KEY_END*		#X108)
(defvar *SL_KEY_A1*		#X109)
(defvar *SL_KEY_A3*		#X10A)
(defvar *SL_KEY_B2*		#X10B)
(defvar *SL_KEY_C1*		#X10C)
(defvar *SL_KEY_C3*		#X10D)
(defvar *SL_KEY_REDO*		#X10E)
(defvar *SL_KEY_UNDO*		#X10F)
(defvar *SL_KEY_BACKSPACE*	#X110)
(defvar *SL_KEY_ENTER*		#X111)
(defvar *SL_KEY_IC*		#X112)
(defvar *SL_KEY_DELETE*		#X113)

(defvar *SL_KEY_F0*	   	   #X200)
(defvar *SL_KEY_F1*		   #X201)
(defvar *SL_KEY_F2*		   #X202)
(defvar *SL_KEY_F3*		   #X203)
(defvar *SL_KEY_F4*		   #X204)
(defvar *SL_KEY_F5*		   #X205)
(defvar *SL_KEY_F6*		   #X206)
(defvar *SL_KEY_F7*		   #X207)
(defvar *SL_KEY_F8*		   #X208)
(defvar *SL_KEY_F9*		   #X209)


(defparameter *special-keys*
  '((:escape . 27)
    (:rubout . 127)))

(defmethod encode-key ((screen cl-screen) key)
  (etypecase key
    (character (char-code key))
    (symbol (cond
              ((assoc key *special-keys*)
               (cdr (assoc key *special-keys*)))
              (t
               (error "Don't know ~S" key))))
    (list (cond
            ((eql (length key) 2)
             (if (eql (car key) :control)
                 (cond ((<= (char-code #\a) (char-code (second key))
                            (char-code #\z))
                        (+ 1 (- (char-code (second key))
                                (char-code #\a))))
                       ((<= (char-code #\A) (char-code (second key))
                            (char-code #\Z))
                        (+ 1 (- (char-code (second key))
                                (char-code #\A))))
                       (t (error "Can't encode key ~S" key)))
                 (error "Can't encode key ~S" key)))
            ((eql (length key) 1)
             (encode-key screen (car key)))
            (t
             (error "Can't encode key ~S" key))))))

(defmethod decode-key ((screen cl-screen) keysym)
  (if (< keysym 255)
      (let ((char (code-char keysym)))
        (if (graphic-char-p char) char
            (cond
              ((<= 1 keysym 26)
               (list :control (code-char (+ (char-code #\a) keysym -1))))
              ((rassoc keysym *special-keys*)
               (car (rassoc keysym *special-keys*)))
              (t
               :unknown))) 
      :unknown)))

;; shame to lose this functionality, but we can't do this portably
;; (see sbcl.lisp if you are using sbcl)
;; (defun %input-function (screen)
;;   ;;; right now just do wacky stuff
;;   (loop while (not (zerop (slang-input-pending 0)))
;;         for key-hook = (or *key-hook*
;;                             (constantly nil))
;;         do (progn
;;              (funcall key-hook (decode-key screen (setf *last-key* (slang-getkey))))
;;              #+nil
;;              (push (decode-key screen *last-key*) *keys*))))

(defmethod get-decoded-key ((screen cl-screen))
  (when (not (zerop (slang-input-pending 0)))
    (decode-key screen (slang-getkey))))

(defvar *color-hash*)

(defparameter *backgrounds*
  '("black" "red" "green" "brown" "blue" "magenta" "cyan" "lightgray"))

(defparameter *color-names*
  (append *backgrounds*
          '("gray" "brightred" "brightgreen" "yellow" "brightblue" 
	    "brightmagenta" "brightcyan" "white")))

(defun random-element (l &key not)
  (loop for e = (elt l (random (length l)))
       if (not (equalp e not))
       return e))

(defun initialize-color-hash ()
  (setf *color-hash* (make-hash-table :test #'equalp))
  (loop for i in *color-names* with c = 1 
		do
		(loop for j in *backgrounds*
			  do (progn
				   (incf c)
				   (setf (gethash (cons i j) *color-hash*) c)
				   (sltt-set-color c (symbol-name (gensym)) i j)))))

(defmethod initialize-screen ((screen cl-screen) &key (abort-char-code -1))
  (unless *initialized*
    (sltt-get-terminfo)
    (slkp-init)
    (sltt-set-cursor-visibility 0)
    (slang-init-tty abort-char-code 0 0)
    (slsmg-init-smg)
    (initialize-color-hash)
    (setf *screen-rows* sltt-screen-rows)
    (setf *screen-cols* sltt-screen-cols)
    (setf *initialized* t)))

(defmethod release-screen ((screen cl-screen))
  (slang-reset-tty)
  (slsmg-reset-smg)
  (setf *initialized* nil))

(defun use-ansi-colors-p ()
  (not (zerop sltt-use-ansi-colors)))

(defmethod clear-screen ((screen cl-screen))
  (slsmg-cls))

(defmethod finish-screen ((screen cl-screen))
  (slsmg-refresh))

(defmethod set-cursor ((screen cl-screen) row col)
  (slsmg-gotorc row col))

(defmethod get-cursor ((screen cl-screen))
  (values
   (slsmg-get-row)
   (slsmg-get-column)))

(defmethod cursor-back ((screen cl-screen))
  (multiple-value-bind
        (row col)
      (get-cursor screen)
    (set-cursor screen row (max (1- col) 0))))

(defun set-cursor-visibility (visible)
  (let ((visflag (if visible 1 0)))
    (sltt-set-cursor-visibility visflag)))

(defmethod write-string-at-cursor ((screen cl-screen) string)
  (slsmg-write-string (coerce string 'simple-base-string)))

(defmethod erase-from-cursor-to-eol ((screen cl-screen))
  (slsmg-erase-eol))

(defmethod erase-from-cursor-to-eos ((screen cl-screen))
  (slsmg-erase-eos))

(defmethod draw-line-at-cursor ((screen cl-screen) direction length)
  (ecase direction
    (:horizontal
     (slsmg-draw-hline length))
    (:vertical
     (slsmg-draw-vline length))))

(defun get-color-pair-number (fg bg)
  (gethash (cons (symbol-name fg) (symbol-name bg)) *color-hash*))

(defmethod valid-color-p ((screen cl-screen) sym type)
  (and (typep sym 'symbol)
       (member (string-downcase (symbol-name sym))
               (ecase type
                 (:foreground *color-names*)
                 (:background *backgrounds*))
               :test #'string=)))

(defmethod valid-colors ((screen cl-screen) type)
  (mapcar #'(lambda (e) (intern (string-upcase e) :keyword))
          (ecase type
            (:foreground *color-names*)
            (:background *backgrounds*))))

(defun color-code (sym type)
  "(color-code sym type). Given the colour symbol for a :foreground or
:background colour, return the numeric colour-code of the colour"
 (let ((str
	(if (typep sym 'symbol)
	    (string-downcase (symbol-name sym))
	    sym)))
   (position str
	     (ecase type
	       (:foreground *color-names*)
	       (:background *backgrounds*)) :test 'string=)))

(defun code-color (code type)
  "(code-color code type) returns the symbol for the given :foreground
or :background type colour given as a symbol name.  eg (code-color
 0 :foreground)->gray"
  (nth code 
       (ecase type
	 (:foreground *color-names*)
	 (:background *backgrounds*))))				

(defmethod set-color ((screen cl-screen) foreground background)
  (if (and (valid-color-p screen foreground :foreground)
           (valid-color-p screen background :background)
           (not (equalp (symbol-name foreground)
                        (symbol-name background))))
      (progn
        (slsmg-set-color (gethash (cons (symbol-name foreground)
                                        (symbol-name background))
                                  *color-hash*))
        (slsmg-set-char-set 0))
      (error "~S / ~S are not valid foreground / background!"
             foreground background)))

(defmethod set-to-default-color ((screen cl-screen))
  (slsmg-set-color 0))

(defvar *screen-resize-hook* nil)

(defun resize-handler ()
  (sltt-get-screen-size)
  (when (not (or (= *screen-rows* sltt-screen-rows) (= *screen-cols* sltt-screen-cols)))
    (slsmg-reinit-smg)
    (setf *screen-rows* sltt-screen-rows)
    (setf *screen-cols* sltt-screen-cols)
    (if *screen-resize-hook*
	(funcall *screen-resize-hook*))))

(defmethod get-screen-size ((screen cl-screen))
  (values sltt-screen-rows sltt-screen-cols))

(defmethod screen-resize-hook ((screen cl-screen))
  *screen-resize-hook*)

(defmethod (setf screen-resize-hook) (new-hook (screen cl-screen))
  (setf *screen-resize-hook* new-hook))

(defun clear-screen-resize-hook ()
  (setf *screen-resize-hook* nil))

(defun make-attribute (fore-sym back-sym)
  "(make-attribute fore-sym back-sym) Given foreground symbol and
background symbol for colour, return the attribute value e.g
(make-attribute 'black 'red)"
  (logior 
   (color-code fore-sym :foreground)
   (ash (color-code back-sym :background) 8)))

(defun set-colors-from-attribute (attribute)
  "(set-colors-from-attribute attribute) Given an attribute code with
the high byte being the index of the background and the low byte the
foreground, set the appropiate color."
  (slsmg-set-color  
   (gethash (cons 
	     (color-code (logand attribute #XFF) :foreground)
	     (color-code (logand (ash attribute -8) #XFF) :background)) *color-hash*)))
