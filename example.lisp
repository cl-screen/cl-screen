

 
(asdf:oos 'asdf:load-op 'cl-screen)

(in-package :cl-screen)


(defmacro with-window-redisplay (window &rest body)
  `(prog1
     ,@body
     (refresh-window ,window)
     (finish-screen *trek-screen*)))
 
(defparameter *trek-screen* (make-instance 'cl-screen))

(initialize-screen *trek-screen*)

(clear-screen *trek-screen*)

(finish-screen *trek-screen*)

(defparameter *screen-height* 
  (get-screen-size *trek-screen*))

(defparameter *screen-width* 
  (nth-value 1 (get-screen-size *trek-screen*)))

(defparameter *title-window* 
  (make-instance 'cl-screen-window 
		 :screen *trek-screen* 
		 :left-x 0 
		 :right-x (1- *screen-width*)
		 :top-y 0 		     
		 :bottom-y 0))



(defparameter *output-window* 
  (make-instance 'cl-screen-window-stream 
		 :screen *trek-screen*
		 :left-x 0 
		 :right-x (1- *screen-width*) 
		 :top-y 1 
		 :bottom-y (- *screen-height* 2)))


(defparameter *input-window*
  (make-instance 'cl-screen-input-stream
		 :screen *trek-screen*
		 :screen-row  *screen-height*
		 :prompt ">"))


(defconstant +help-file+ "startrek.hlp")
(defconstant +pict-file+ "startrek.pic")
(defconstant +map-file+  "startrek.map")

(defun more-p ()
  (setf (prompt-of *input-window*) "More?")
  (read-line *input-window*))

(defun display-file (file-name stream)
  (let ((p (parse-namestring file-name)))
    (with-open-file (s p :direction :input) 
      (loop
         for line = (read-line s) then (read-line s nil nil)
         for index = 0 then (1+ index)
         while line
         do
           (with-window-redisplay stream
             (write-line line stream))
           (if (= (mod index (height-of stream))
                  (1- (height-of stream)))
               (more-p))))))


(defun confirm-p (format-string &rest arguments)
  (setf (prompt-of *input-window*)
	 (apply #'format (cons nil (cons format-string arguments))))
  (let ((response (read-line *input-window*)))
     (and 
      (not (zerop (length response))) 
      (char-equal (char response 0) #\Y))))
    
(defun number-query (format-string &rest arguments)
  (setf (prompt-of *input-window*)
	 (apply #'format (cons nil (cons format-string arguments))))
  (let ((response (read-line *input-window*)))
     (parse-integer response :junk-allowed t)))

(defun value-query (format-string &rest arguments)
  (setf (prompt-of *input-window*)
	 (apply #'format (cons nil (cons format-string arguments))))
  (let ((response (read-line *input-window*)))
     (parse-float response :junk-allowed t)))


(defconstant +region-names+ 
    (list "ANTARES" "SIRUS"
	  "RIGEL" "DENEB"
	  "POCYON" "CAPELLA"
	  "VEGA" "BETENGULUSE"
	  "CANAPUS" "ALDEBARAN"
	  "ALTAIR" "REGULUS"
	  "SAGITATTARIUS" "ARCTURUS"
	  "POLLUX" "SPICA")
  "Names of the quadrants regions")

(defconstant +quadrant-names+ (list "I" "II" "III" "IV"))

(defstruct sector 
  "Structure describing the data of each sector."
  (scannedp)
  (cobolarg-list)
  (starbase-list) 
  (star-list))

(defvar *quadrants* (make-array '(8 8)))
 
(defstruct location
  (quad-x 0)
  (quad-y 0)
  (sect-x 0)
  (sect-y 0)) 

(defvar *lambda-location* (make-location))

(defvar *start-cobolargs* 0)
(defvar *total-cobolargs* 0)
(defvar *total-starbases* 0)

(defconstant +start-energy+ 3000)
(defconstant +start-photon-torpedoes+ 10)
(defconstant +min-cobolarg-shield-energy+ 100)
(defconstant +max-cobolarg-shield-energy+ 300)
(defconstant +min-cobolarg-phaser-fire+ 50)
(defconstant +max-cobolarg-phaser-fire+ 200)
(defconstant +max-stars-sector+ 9)
(defconstant +min-cobolargs-galaxy+ 7)
(defconstant +max-cobolargs-galaxy+ 28)
(defconstant +max-starbases-galaxy+ 5)
(defconstant +min-days-mission+ 15)
(defconstant +max-days-mission+ 45)
(defconstant +start-star-date+  3000)

(defvar *energy* +start-energy+)
(defvar *shield-energy* 0)
(defvar *remaining-days* nil)
(defvar *end-date* 0)
(defvar *photon-torpedoes* +start-photon-torpedoes+)
(defvar *lost-game* nil)
(defvar *won-game* nil)

(defvar *warp-engine-status* 0)
(defvar *srs-sensor-status* 0)
(defvar *lrs-sensor-status* 0)
(defvar *phaser-status* 0)
(defvar *photon-torpedo-status* 0)
(defvar *damage-control-status* 0)
(defvar *shield-control-status* 0)
(defvar *library-computer-status* 0)

(defun parse-number (number-string)
   (let ((number 0))
     (setf number (read-from-string number-string))
     (if (numberp number) number nil)))

(defun get-historical-name (x y)
  (concatenate 'string (nth (+ (* y 2) (if (> x 3) 1 0)) +region-names+) 
               " "
               (nth (mod x 4) +quadrant-names+)))


(defun setup-sectors ()
   (dotimes (j 8)
     (dotimes (i 8)
       (setf (aref *quadrants* i j) (make-sector)))))

(defun fill-stars ()
   (dotimes (j 8)
     (dotimes (i 8)
      (dotimes (k (1+ (random +max-stars-sector+)))
	(let ((x (random 8)) (y (random 8)))
	  (setf (sector-star-list (aref *quadrants* i j))
		(adjoin (list x y)
			(sector-star-list (aref *quadrants* i j)))))))))

(defun fill-cobolargs ()
   (progn
     (setq *total-cobolargs*
	   (+ +min-cobolargs-galaxy+
	      (random (1+ 
		       (- +max-cobolargs-galaxy+ +min-cobolargs-galaxy+)))))
     (setq *start-cobolargs* *total-cobolargs*)
     (dotimes (k *total-cobolargs*)
       (let ((quad-x (random 8)) (quad-y (random 8))
 	    (sect-x (random 8)) (sect-y (random 8))
 	    (shield-strength (+  +min-cobolarg-shield-energy+
 				 (random (- +max-cobolarg-shield-energy+
 					    +min-cobolarg-shield-energy+)))))
 	(setf (sector-cobolarg-list (aref *quadrants* quad-x quad-y))
 	      (cons (list sect-x sect-y shield-strength)
 		    (sector-cobolarg-list (aref *quadrants* quad-x quad-y))))))))

(defun fill-starbases ()
   (progn
     (setq *total-starbases* (1+ (random +max-starbases-galaxy+)))
     (dotimes (k *total-starbases*)
       (let ((quad-x (random 8)) (quad-y (random 8))
 	    (sect-x (random 8)) (sect-y (random 8)))
 	(setf (sector-starbase-list (aref *quadrants* quad-x quad-y))
 	      (adjoin (list sect-x sect-y)
 		      (sector-starbase-list (aref *quadrants* quad-x quad-y))))))))

(defun setup-galaxy ()
  (progn
    (setup-sectors)
    (fill-stars)
    (fill-cobolargs)
    (fill-starbases)
    (setf *remaining-days* 
	  (+ (random (- +max-days-mission+ +min-days-mission+))
	     +min-days-mission+))
    (setf *end-date* (+ +start-star-date+ *remaining-days*))))
    

(defun setup-lambda-location ()
  (setf *lambda-location* 
    (make-location 
     :quad-x (random 8)
     :quad-y (random 8)
     :sect-x (random 8)
     :sect-y (random 8))))

(defun quadrant-matrix (quad-x quad-y)
  (let ((scan-array 
	 (make-array '(8 8) 
		     :element-type '(unsigned-byte 8) 
		     :initial-element 0))
	(cobolarg-list (sector-cobolarg-list (aref *quadrants* quad-x quad-y)))
	(starbase-list (sector-starbase-list (aref *quadrants* quad-x quad-y)))
	(star-list (sector-star-list (aref *quadrants* quad-x quad-y))))
    (loop for (i j) in  star-list do
	  (setf (aref scan-array i j) 1))
    (loop for (i j) in  starbase-list do
	  (setf (aref scan-array i j) 2))
    (loop for (i j) in cobolarg-list do
	  (setf (aref scan-array i j) 3))
    (when (and (= quad-x (location-quad-x *lambda-location*))
	       (= quad-y (location-quad-y *lambda-location*)))
      (setf (aref scan-array 
		  (location-sect-x *lambda-location*)
		  (location-sect-y *lambda-location*)) 
	4))
    scan-array))

(defun report-condition (quad-x quad-y)
  (if (> (length (sector-cobolarg-list (aref *quadrants* quad-x quad-y))) 0)
	  "*RED*"
	  "GREEN"))

(defun get-symbol (number)
  (case number
    (0 "   ")
    (1 " * ")
    (2 ">!<")
    (3 "+K+")
    (4 "<*>")))
	    
(defun report-status (quad-x quad-y line)
  (let ((here? (and (= quad-x (location-quad-x *lambda-location*))
		    (= quad-y (location-quad-y *lambda-location*)))))
    (case line
      (0 (format nil "Stardate           ~D" 
		 (- *end-date* *remaining-days*)))
      (1 (format nil "Condition          ~A" 
                 (report-condition quad-x quad-y)))
      (2 (if here?
	     (format nil "Quadrant           ~D , ~D"
                 (1+ (location-quad-x *lambda-location*))
                 (1+ (location-quad-y *lambda-location*)))
         "Quadrant           N/A"))
      (3 (if here?
             (format nil "Sector             ~D , ~D"
                     (1+ (location-sect-x *lambda-location*))
                     (1+ (location-sect-y *lambda-location*)))
             "Sector             N/A"))
      (4 (format nil "Photon torpedoes   ~D"
                 *photon-torpedoes*))
      (5 (format nil "Total energy       ~D"
                 (+ *shield-energy* *energy*)))
      (6 (format nil "Shields            ~D"
                 *shield-energy*))
      (7 (format nil "Cobolargs remaining ~D"
                 *total-cobolargs*)))))

(defun short-range-sensor-scan ()
  (with-window-redisplay *output-window*
    (let* ((quad-x (location-quad-x *lambda-location*))
	   (quad-y (location-quad-y *lambda-location*))
	   (grid (quadrant-matrix quad-x quad-y)))
      (setf (sector-scannedp (aref *quadrants* quad-x quad-y)) t)
      (when (string= (report-condition quad-x quad-y) "*RED*")
      (write-line  "COMBAT AREA  CONDITION RED" *output-window*))
      (write-line  "------------------------------" *output-window*)
    (dotimes (j 8)
      (write-string (get-symbol 0) *output-window*)
      (dotimes (i 8)
	(write-string (get-symbol (aref grid i j)) *output-window*))
      (write-string (get-symbol 0) *output-window*)
      (write-line (report-status quad-x quad-y j) *output-window*))
    (write-line "------------------------------" *output-window*)
    nil)))

(defun scan-quadrant (quad-x quad-y)
  (if (sector-scannedp (aref *quadrants* quad-x quad-y))
      (let ((cobolargs (length (sector-cobolarg-list 
			       (aref *quadrants* quad-x quad-y))))
	    (starbases (length (sector-starbase-list
				(aref *quadrants* quad-x quad-y))))
	    (stars (length (sector-star-list
			    (aref *quadrants* quad-x quad-y)))))
	(format nil "~1D~1D~1D" cobolargs starbases stars))  "***"))

(defun long-range-sensor-scan ()
  (with-window-redisplay *output-window*
    (let ((quad-x (location-quad-x *lambda-location*))
	  (quad-y (location-quad-y *lambda-location*)))
      (write-line "-------------------" *output-window*)
      (loop for j from (1- quad-y) to (1+ quad-y) do
	   (loop for i from (1- quad-x) to (1+ quad-x) do
		(if (and  (>= j 0) (< j 8) (>= i 0) (< i 8))
		    (progn
		      (setf (sector-scannedp (aref *quadrants* i j)) t)
		      (format *output-window* ": ~A " (scan-quadrant i j)))
		    (write-string ": *** " *output-window*)))
	   (format *output-window* ":~%"))
      (write-line "-------------------" *output-window*))))

(defun cumulative-galacic-record ()
  (with-window-redisplay *output-window*
    (terpri *output-window*)
    (write-line "            COMPUTER RECORD FOR GALAXY            " *output-window*)
    (format *output-window*
	    "~&            Lambda position: ~D , ~D~%" 
	    (1+ (location-quad-x *lambda-location*))
	    (1+ (location-quad-y *lambda-location*)))
    (write-line "     1     2     3     4     5     6     7     8  " *output-window*)
    (write-line "   ----- ----- ----- ----- ----- ----- ----- -----" *output-window*)
    (dotimes (j 8)
      (format *output-window* "~&~1D  " (1+ j))
      (dotimes (i 8)
	(format *output-window* " ~3A  " (scan-quadrant i j)))
      (terpri)
      (write-line "   ----- ----- ----- ----- ----- ----- ----- -----" *output-window*))))

(defun calculate-relative-location (course warp-factor)
  (progn
    (assert (or (> course 0) (< course 9)
		(> warp-factor 0) (< warp-factor 9)))
    (let* ((ratio (* (1- course) (/ pi 4)))
	   (rel-x (* (cos ratio) warp-factor))
	   (rel-y (* (sin ratio) warp-factor)))
      (multiple-value-bind (den-x rem-x) (truncate rel-x)
	(multiple-value-bind (den-y rem-y) (truncate rel-y)
	  (make-location 
	   :quad-x den-x
	   :quad-y (- den-y)
	   :sect-x (truncate (* rem-x 8))
	   :sect-y (- (truncate (* rem-y 8)))))))))
	 
(defun calculate-relative-course (pos)
  (let* ((fact-x (+ (* (location-quad-x pos) 8) (location-sect-x pos)))
	 (fact-y (- (+ (* (location-quad-y pos) 8) (location-sect-y pos))))
	 (-x? (if (< fact-x 0) t nil)) (-y? (if (< fact-y 0) t nil))
	 (cource! (if (= fact-x 0)
		      (if -y? 7 3)
		    (if (= fact-y 0)
			(if -x? 5 1)
		      (let ((rho (atan (/ (abs fact-y) (abs fact-x))))
			    (theta (if -x?
				       (if -y? pi (* 1/2 pi))
				     (if -y? (* 3/2 pi) 0))))
			(1+ (* (+ rho theta) (/ 4 pi)))))))
	 (warp-factor (/ (sqrt (+ (* fact-x fact-x) (* fact-y fact-y))) 8)))
    (list cource! warp-factor)))

(defun add-locations (first-pos second-pos)
  (let ((fact-x (+
		 (+ (* 8 (location-quad-x first-pos)) (location-sect-x first-pos))
		 (+ (* 8 (location-quad-x second-pos)) (location-sect-x second-pos))))
	(fact-y (+
		 (+ (* 8 (location-quad-y first-pos)) (location-sect-y first-pos))
		 (+ (* 8 (location-quad-y second-pos)) (location-sect-y second-pos)))))
    (multiple-value-bind (quad-x sect-x) (truncate (/ fact-x 8))
      (multiple-value-bind (quad-y sect-y) (truncate (/ fact-y 8))
	(make-location :quad-x quad-x :quad-y quad-y
		       :sect-x (* sect-x 8) :sect-y (* sect-y 8))))))

(defun sub-locations (first-pos second-pos)
  (let ((fact-x (-
		 (+ (* 8 (location-quad-x first-pos)) (location-sect-x first-pos))
		 (+ (* 8 (location-quad-x second-pos)) (location-sect-x second-pos))))
	(fact-y (-
		 (+ (* 8 (location-quad-y first-pos)) (location-sect-y first-pos))
		 (+ (* 8 (location-quad-y second-pos)) (location-sect-y second-pos)))))
    (multiple-value-bind (quad-x sect-x) (truncate (/ fact-x 8))
      (multiple-value-bind (quad-y sect-y) (truncate (/ fact-y 8))
	(make-location :quad-x quad-x :quad-y quad-y
		       :sect-x (* sect-x 8) :sect-y (* sect-y 8))))))

(defun location= (loc1 loc2)
  (let ((loc1-quad-x (location-quad-x loc1))
	(loc1-quad-y (location-quad-y loc1))
	(loc1-sect-x (location-sect-x loc1))
	(loc1-sect-y (location-sect-y loc1))
	(loc2-quad-x (location-quad-x loc2))
	(loc2-quad-y (location-quad-y loc2))
	(loc2-sect-x (location-sect-x loc2))
	(loc2-sect-y (location-sect-y loc2)))
    (and (= loc1-quad-x loc2-quad-x)
	 (= loc1-quad-y loc2-quad-y)
	 (= loc1-sect-x loc2-sect-x)
	 (= loc1-sect-y loc2-sect-y))))

(defun warp-engine-control ()
  (block warp-control
    (let ((course)
	  (warp-factor)
	  (energy-consumption)
	  (new-location))
      (setq course (ignore-errors (number-query "COURSE (1-9)?")))

      (unless (and (numberp course) (and (>= course 1) (<= course 9)))
	(with-window-redisplay *output-window*
	  (write-line "Illogical course, Sir!" *output-window*))
	  (return-from warp-control nil))
      (setq warp-factor (ignore-errors (value-query "WARP (0-8)?")))

      (unless (numberp warp-factor)
	(with-window-redisplay *output-window*
	  (write-line "What??" *output-window*)
	  (terpri *output-window*))
	(return-from warp-control nil))
      
      (unless (and (>= warp-factor 0.0) (< warp-factor 9.0))
	(with-window-redisplay *output-window*
	  (format *output-window*
		  "Chief engineer Xophe reports 'The engines won't take warp ~D.'"
		  warp-factor)
	  (terpri *output-window*))
	  (return-from warp-control nil))


      (when (and (> *warp-engine-status* 0.85) (> warp-factor 0.2))
	(with-window-redisplay *output-window*
	  (write-line "Warp-engines are damaged. Maximum speed warp 0.2" *output-window*)
	  (terpri *output-window*))
	(setq warp-factor 0.2))
      
      (setq energy-consumption (round (* warp-factor 8 2)))
      (if (< *energy* energy-consumption)
	  (progn
	    (with-window-redisplay *output-window*
	      (format *output-window*
		      "Engeneering reports 'Insufficient energy for manuvering at warp ~D'"
		      warp-factor)
	      (terpri *output-window*))
	    (return-from warp-control nil))
	(setf *energy* (- *energy* energy-consumption)))
      
      (setq new-location
	    (add-locations *lambda-location*
			   (calculate-relative-location course warp-factor)))
      
      (if (or (< (location-quad-x new-location) 0) 
	      (> (location-quad-x new-location) 7)
	      (< (location-sect-y new-location) 0)
	      (> (location-sect-y new-location) 7))
	  (progn
	    (with-window-redisplay *output-window*
	      (write-line 
	       "Lt. Tilton reports message from starfleet command:" *output-window*)
	      (write-line 
	       "  'Permission to attempt to crossing of galactic perimiter" *output-window*)
	      (write-line "   is hereby *DENIED*. Shut down your engines.'" *output-window*)
	      (terpri *output-window*)
	      (write-line 
	       "Chief engineer Xophe reports 'Warp engines shut down.'" *output-window*) 
	    (terpri *output-window*))
	    (return-from warp-control nil))
	(setf *lambda-location* new-location)))))

(defun calculate-phaser-hit (energy first-pos second-pos)
  (let* ((first-sect-x (first first-pos))
	 (second-sect-x (first second-pos))
	 (first-sect-y (second first-pos))
	 (second-sect-y (second second-pos))
	 (rel-x (- second-sect-x first-sect-x))
	 (rel-y (- second-sect-y first-sect-y))
	 (distance-sqr (+ (* rel-x rel-x) (* rel-y rel-y))))
    (if (= distance-sqr 0) 
	(setf distance-sqr 1))
    (truncate (* (/ energy distance-sqr) (+ (random 2) 2)))))

(defun reduce-cobolargs (number)
  (decf *total-cobolargs* number)
  (when (<= *total-cobolargs* 0)
    (with-window-redisplay *output-window*
      (terpri *output-window*)
      (write-line
       "Congratulations captain! The last Cobolarg battleship that has been" *output-window*)
      (write-line "menacing the galaxy has been destroyed." *output-window*)
      (format *output-window*
		"~%Your efficiency rating is ~3,2F.~2%"
		(* 1000 (/ *start-cobolargs* *remaining-days*))))
    (setf *won-game* t)))

(defun phaser-control ()
  (block phaser
    (let ((cobolarg-list (sector-cobolarg-list 
			 (aref *quadrants* 
			       (location-quad-x *lambda-location*)
			       (location-quad-y *lambda-location*))))
	  (alotted-energy 0)
	  (selection))

      (when (not cobolarg-list)
	(with-window-redisplay *output-window*
	  (terpri *output-window*)
	  (write-line 
	   "Science officer Xach reports: no enemy ships in this quadrant" *output-window*)
	  (terpri *output-window*))
	(return-from phaser nil))
    
      (when (> *phaser-status* .85)
	(with-window-redisplay *output-window*
	  (terpri *output-window*)
	  (write-line "Phasers inoperative." *output-window*)
	  (terpri *output-window*)))
    
      (if (= (length cobolarg-list) 1)
	 (progn
	   (with-window-redisplay *output-window*	  
	     (terpri *output-window*)
	    (write-line "Phasers locked on target." *output-window*)
	    (setq selection 0)
	    (terpri *output-window*)
	    (write-line "Select one of the following targets:" *output-window*))
	
	   (let ((number 0))
	     (with-window-redisplay *output-window*
	      (loop for (i j) in cobolarg-list do
		   (format *output-window*
			   "~&Cobolarg ~D at ~D , ~D~%" number (1+ i) (1+ j))
		   (incf number))
	      (decf number)
	      (terpri *output-window*))
	     (setq selection (ignore-errors (number-query  "Selection? ")))
	      
	     (unless (numberp selection)
	       (with-window-redisplay *output-window*
		 (write-line "What??" *output-window*))
	       (return-from phaser nil))
	      
	     (when (or (< selection 0) (> selection number))
	       (with-window-redisplay *output-window*
		 (terpri *output-window*)
		 (write-line 
		  "Ensign Pitman reports 'No ship with that number'" *output-window*))
	      (return-from phaser nil))
    
	     (with-window-redisplay *output-window*
	       (format *output-window*
		       "~&Energy available = ~D units~%" *energy*))
	     (setq alotted-energy 
		   (ignore-errors 
		     (value-query "Number of units to fire? ")))
	     (when (or (not (numberp alotted-energy)) (< alotted-energy 0.0))
	       (with-window-redisplay *output-window*
		 (write-line "What??" *output-window*))
	       (return-from phaser nil))
      
	     (when (> alotted-energy *energy*)
	       (with-window-redisplay *output-window*
		 (terpri *output-window*)
		 (write-line 
		  "Ensign Pitman reports: 'Insufficient energy.'" *output-window*))
	       (return-from phaser nil))
      
	     (setf *energy* (- *energy* alotted-energy))
	      
	     (let* ((cobolarg (nth selection cobolarg-list))
		    (lambda-pos (list (location-sect-x *lambda-location*)
					  (location-sect-y *lambda-location*)))
		    (cobolarg-pos-x (first cobolarg))
		    (cobolarg-pos-y (second cobolarg))
		    (cobolarg-shield (third cobolarg))
		    (hit (calculate-phaser-hit 
			  alotted-energy lambda-pos cobolarg)))
	       (if (> hit (* .15 cobolarg-shield))
		   (with-window-redisplay *output-window*
		     (terpri *output-window*)
		     (format *output-window*
			     "~&~D units hit Cobolarg at ~D , ~D~%"
			     (ceiling hit) (first cobolarg) (second cobolarg))
		     (setq cobolarg-shield (- cobolarg-shield hit))
		     (if (< cobolarg-shield 0)
			 (with-window-redisplay *output-window*
		    (write-line "*** COBOLARG DESTROYED ***" *output-window*)
		    (setq cobolarg-list (remove cobolarg cobolarg-list))
		    (reduce-cobolargs 1))
		  (with-window-redisplay *output-window*
		    (format *output-window*
			    "  (Sensor show ~D units remaining.)~%" 
			    (ceiling cobolarg-shield))
		    (setf (third (nth selection cobolarg-list))
			  cobolarg-shield))))

		   (setf (sector-cobolarg-list
			  (aref *quadrants* 
			 (location-quad-x *lambda-location*)
			 (location-quad-y *lambda-location*)))
		  cobolarg-list))

	       (with-window-redisplay *output-window*
		 (format *output-window*
			 "~&Sensors show no damage at ~D , ~D~%"
			 (1+ cobolarg-pos-x) (1+ cobolarg-pos-y))))))))))

(defun lambda-destroyed ()
  (with-window-redisplay *output-window*
    (terpri *output-window*)
    (write-line "The Lambda has been destroyed!" *output-window*)
    (write-line "Soon Lisp will be forever forgotten." *output-window*)
    (write-line "The game is over." *output-window*)
    (format *output-window*
	    "~&At the end of the game there were ~D cobolarg~P remaining.~2%"
	    *total-cobolargs* *total-cobolargs*)
    (setf *lost-game* t)))

(defun calculate-lambda-damage (energy-hit)
  (when (< (- *shield-energy* energy-hit) 0) 
    (lambda-destroyed)
    (return-from calculate-lambda-damage))
  (when (> energy-hit (* *shield-energy* .15))
    (let ((damage (/ (random 100) 100))
	  (system (random 8))
	  (sensor-name "None"))
      (macrolet ((update (variable name)
		   `(progn (incf damage ,variable)
				   (if (> damage 1.0) (setf damage 1.0))
				   (setf ,variable damage)
				   (setf sensor-name ,name))))
	(case system
	  (0 (update *warp-engine-status* "warp engines"))
	  (1 (update *srs-sensor-status* "short range scanner"))
	  (2 (update *lrs-sensor-status* "long range scanner"))
	  (3 (update *phaser-status*      "phaser control"))
	  (4 (update *photon-torpedo-status* "photon torpedo control"))
	  (5 (update *damage-control-status* "damage control"))
	  (6 (update *shield-control-status* "shield control"))
	  (7 (update *library-computer-status* "library computer")))
	(let ((damage-extent "damaged"))
	  (when (> damage .85)
	    (setf damage-extent "disabled"))
	  (with-window-redisplay *output-window*
	    (format *output-window*
		    "~&Damage control reports '~A ~A by the hit'~%"
		    sensor-name damage-extent))))
      (decf *shield-energy* energy-hit))))

(defun complete-repair-lambda-damage ()
  (setf *warp-engine-status* 0)
  (setf *srs-sensor-status* 0)
  (setf *lrs-sensor-status* 0)
  (setf *phaser-status* 0)
  (setf *photon-torpedo-status* 0)
  (setf *damage-control-status* 0)
  (setf *shield-control-status* 0)
  (setf *library-computer-status* 0))

(defun partial-repair-lambda-damage ()
  (let ((repair (/ (random 100) 100)))
    (macrolet ((update (variable)
                       `(if (> ,variable 0)
			    (let ((damage (- ,variable repair)))
			      (if (< damage 0) (setf damage 0))
			      (setf ,variable damage)
			      (return-from partial-repair-lambda-damage)))))
      (update *warp-engine-status*)
      (update *shield-control-status*)
      (update *srs-sensor-status*)
      (update *lrs-sensor-status*)
      (update *phaser-status*)
      (update *photon-torpedo-status*)
      (update *damage-control-status*)
      (update *library-computer-status*))))

(defun found-cobolargs-quadrant ()
  (let ((cobolarg-list 
	 (sector-cobolarg-list
	  (aref *quadrants* 
		(location-quad-x *lambda-location*)
		(location-quad-y *lambda-location*)))))
    (if (> (list-length cobolarg-list) 0)
	t nil)))
      
(defun cobolarg-phaser-fire ()
  (block phaser
    (let ((cobolarg-list 
	   (sector-cobolarg-list
	    (aref *quadrants* 
		  (location-quad-x *lambda-location*)
		  (location-quad-y *lambda-location*))))
	  (lambda-sect-x (location-sect-x *lambda-location*))
	  (lambda-sect-y (location-sect-y *lambda-location*)))
    
      (loop for (sect-x sect-y) in cobolarg-list do
	    (let ((cobolarg-shoots (if (= (random 2) 0) nil t)))
	      (when cobolarg-shoots
		(let* ((energy-fired (+ +min-cobolarg-phaser-fire+
					(random (1+
						 (- +max-cobolarg-phaser-fire+
						    +min-cobolarg-phaser-fire+)))))
		       (energy-hit (calculate-phaser-hit 
				    energy-fired
				    (list lambda-sect-x lambda-sect-y)
				    (list sect-x sect-y))))
		  (with-window-redisplay *output-window*
		    (format *output-window*
			    "~%~D unit hit on Lambda from ~D, ~D~%"
			    energy-hit (1+ sect-x) (1+ sect-y))
		    (calculate-lambda-damage  energy-hit)))))))))

(defun photon-torpedo-control ()
  (block torpedo
    (unless (> *photon-torpedoes* 0)
      (with-window-redisplay *output-window*
	(write-line "All photon torpedoes expended." *output-window*)
	(terpri *output-window*))
      (return-from torpedo nil ))
    
    (when (> *photon-torpedo-status* .85)
      (with-window-redisplay *output-window*
	(write-line "Photon tubes are not operational" *output-window*)
	(terpri *output-window*))
      (return-from torpedo nil))
    
    (let ((course))
      (setq course (ignore-errors (number-query "COURSE (1-9)? ")))

      (unless (numberp course)
	(with-window-redisplay *output-window*
	  (write-line "What??" *output-window*))
	(return-from torpedo nil))
      
      (unless (and (>= course 1) (<= course 9))
	(with-window-redisplay *output-window*
	  (terpri *output-window*)
	  (write-line "Ensign Pitman reports 'incorrect course data, sir!'" *output-window*))
	  (return-from torpedo nil))

      (decf *photon-torpedoes*)
      (with-window-redisplay *output-window*
	(terpri *output-window*)
	(write-line "Torpedo track:" *output-window*))
      
      (let* ((ratio (* (/ (1- course) 4) pi))
	     (step-x (cos ratio))
	     (step-y (- (sin ratio)))
	     (cur-x (location-sect-x *lambda-location*))
	     (cur-y (location-sect-y *lambda-location*))
	     (sector (aref *quadrants*
			   (location-quad-x *lambda-location*)
			   (location-quad-y *lambda-location*)))
	     (star-list (sector-star-list sector))
	     (starbase-list (sector-starbase-list sector))
	     (cobolarg-list (sector-cobolarg-list sector)))
	(loop

	 (setq cur-x (+ cur-x step-x))
	 (setq cur-y (+ cur-y step-y))
	  
	 (dolist (s star-list)
	   (when (and (= (first s) (round cur-x)) 
		      (= (second s) (round cur-y)))
	     (with-window-redisplay *output-window*
	       (format *output-window*
		       "~&Star at ~D , ~D absorbed torpedo energy.~%"
		       (1+ (first s)) (1+ (second s)))
	       (terpri *output-window*))
	     (return-from torpedo nil)))

	   (dolist (k cobolarg-list)
	     (when (and (= (first k) (round cur-x)) 
			(= (second k) (round cur-y)))
	     (with-window-redisplay *output-window*
	       (write-line "*** COBOLARG DESTROYED ***" *output-window*)
	       (terpri *output-window*))
	     (setf (sector-cobolarg-list
		    (aref *quadrants*
			  (location-quad-x *lambda-location*)
			  (location-quad-y *lambda-location*)))
		   (remove k cobolarg-list))
	     (reduce-cobolargs 1)
	     (return-from torpedo nil)))

	   (dolist (s starbase-list)
	     (when (and (= (first s) (round cur-x)) 
			(= (second s) (round cur-y)))
	       (with-window-redisplay *output-window*
		 (write-line "*** STARBASE DESTROYED ***" *output-window*))
	       (setf (sector-starbase-list
		      (aref *quadrants*
			    (location-quad-x *lambda-location*)
			    (location-quad-y *lambda-location*)))
		     (remove s starbase-list))
	       (decf *total-starbases*)
	       (if (= *total-starbases* 0)
		   (progn
		     (with-window-redisplay *output-window*
		       (write-line "That does it, captain!! You are hereby relieved of command" 
				   *output-window*)
		       (write-line "and sentenced to 99 days hard labor on JEEEv1.0!" 
				   *output-window*)
		       (setf *lost-game* t))
		     (return-from torpedo nil))
		   (progn
		     (with-window-redisplay *output-window*
		       (write-line 
			"Starfleet command is reviewing your record" *output-window*)
		       (write-line "to consider court martial." *output-window*)
		       (terpri *output-window*))
		     (return-from torpedo nil)))))
	       

	   (when (or (< cur-x 0) (> cur-x 7)
		     (< cur-y 0) (> cur-y 7))
	     (with-window-redisplay *output-window*
	       (write-line "Torpedo missed." *output-window*)
	       (terpri *output-window*))
	     (return-from torpedo nil))

	   (with-window-redisplay *output-window*
	     (format *output-window* "                ~D , ~D~%" 
		     (1+ (round cur-x)) (1+ (round cur-y)))))))))

(defun shield-control ()
  (block shield
    (let ((allotted-energy 0))
      
      (when (> *shield-control-status* .85)
	(with-window-redisplay *output-window*
	  (write-line "Shields inoperable." *output-window*))
	(return-from shield nil))

      (with-window-redisplay *output-window*
	(format *output-window* 
		"~&Energy currently allotted to shields = ~D units~%" 
		*shield-energy*)
	(format *output-window* "~&Energy available = ~D units~%" *energy*))


      (setq allotted-energy 
	    (ignore-errors (value-query "Number of units to shields? ")))

      (when (or (not (numberp allotted-energy)) (< allotted-energy 0))
	(with-window-redisplay *output-window*
	  (write-line "What??" *output-window*))
	(return-from shield nil))
    
      (when (= allotted-energy *shield-energy*)
	(with-window-redisplay *output-window*
	  (write-line "" *output-window*))
	(return-from shield nil))

      (when (> allotted-energy (+ *energy* *shield-energy*))
	(with-window-redisplay *output-window*
	  (write-line 
	   "Deflector control reports 'This is not the federation treasury'" *output-window*))
	(return-from shield nil))
      
      (decf *energy* (- allotted-energy *shield-energy*))
      (setf *shield-energy* allotted-energy)
      
      (with-window-redisplay *output-window*
	(write-line "Deflector control room reports:" *output-window*)
	(format *output-window*
		"'Shields are now at ~D units at your commmand.'~%"
		*shield-energy*))
      *shield-energy*)))

(defun damage-control-report ()
  (with-window-redisplay *output-window*
    (terpri *output-window*)
    (write-line "Device              State of repair" *output-window*)
    (write-line "-----------------------------------" *output-window*)
    (format *output-window*
	    "Warp engines              ~3,2F~%" *warp-engine-status*)
    (format *output-window*
	    "Short range sensor        ~3,2F~%" *srs-sensor-status*)
    (format *output-window*
	    "Long range sensor         ~3,2F~%" *lrs-sensor-status*)
    (format *output-window*
	    "Phaser control            ~3,2F~%" *phaser-status*)
    (format *output-window*
	    "Photon tubes              ~3,2F~%" *photon-torpedo-status*)
    (format *output-window*
	    "Shield control            ~3,2F~%" *shield-control-status*)
    (format *output-window*
	    "Damage control            ~3,2F~%" *damage-control-status*)
    (format *output-window*
	    "Library computer          ~3,2F~%" *library-computer-status*)
    (terpri *output-window*)))

(defun photon-torpedo-data ()
  (block data
    (let ((cobolarg-list (sector-cobolarg-list 
			 (aref *quadrants* 
			       (location-quad-x *lambda-location*)
			       (location-quad-y *lambda-location*)))))
      (when (= (length cobolarg-list) 0)
	(with-window-redisplay *output-window*
	  (terpri *output-window*)
	  (write-line "Sensors report no cobolargs in this quadrant" *output-window*)
	  (terpri *output-window*))
	(return-from data nil))

      (with-window-redisplay *output-window*
	(format *output-window*
		"~%Distance to cobolarg battle cruiser~P~2%" 
		(length cobolarg-list)))
      
      (loop for (i j) in  cobolarg-list do
	    (let ((data (calculate-relative-course 
			 (sub-locations
			  (make-location 
			   :quad-x (location-quad-x *lambda-location*)
			   :quad-y (location-quad-y *lambda-location*)
			   :sect-x i :sect-y j)
			  *lambda-location*))))

	      (with-window-redisplay *output-window*
		(format *output-window* 
			"~&Cobolarg in sector ~D , ~D~%Course = ~3,2F~%Distance= ~3,2F~2%"
		      (1+ i) (1+ j) (first data) (second data))))))))
	      
(defun starbase-nav-data ()
  (block data
    (let ((starbase-list (sector-starbase-list 
			  (aref *quadrants* 
				(location-quad-x *lambda-location*)
				(location-quad-y *lambda-location*)))))

      (when (= (length starbase-list) 0)
	(with-window-redisplay *output-window*
	  (terpri *output-window*)
	  (write-line "Sensors report no starbase in this quadrant" *output-window*)
	  (terpri *output-window*))
	  (return-from data nil))

      (with-window-redisplay *output-window*
	(format *output-window*
		"~%Distance to starbase~P~2%" 
		(length starbase-list)))
      
      (loop for (i j) in  starbase-list do
	    (let ((data (calculate-relative-course 
			 (sub-locations
			  (make-location 
			   :quad-x (location-quad-x *lambda-location*)
			   :quad-y (location-quad-y *lambda-location*)
			   :sect-x i :sect-y j)
			  *lambda-location*))))

	      (with-window-redisplay *output-window*
		(format *output-window* 
			(concatenate 'string
				     "~&starbase in sector ~D , ~D"
				     "~%Course = ~3,2F~%Distance= ~3,2F~2%")
			(1+ i) (1+ j) (first data) (second data ))))))))

(defun navigation-calculator ()
  (block navigation
    (with-window-redisplay *output-window*
      (terpri *output-window*)
      (write-line "Navigation calculator" *output-window*)
      (format *output-window* "~%Current position is quadrant ~D , ~D sector ~D , ~D~2%"
	      (1+ (location-quad-x *lambda-location*))
	      (1+ (location-quad-y *lambda-location*))
	      (1+ (location-sect-x *lambda-location*))
	      (1+ (location-sect-y *lambda-location*))))
    
    (with-window-redisplay *output-window*
      (write-line "Enter quadrant coordinates" *output-window*))
    (let 
        ((quad-x (ignore-errors 
                   (number-query "X coordinate (1..8): ")))
         (quad-y (ignore-errors 
                   (number-query "Y coordinate (1..8): "))))
      (unless (and (numberp quad-x) (numberp quad-y))
        (with-window-redisplay *output-window*
          (write-line "What??" *output-window*))
        (return-from navigation nil))
      
      (when (or (< quad-x 1) (> quad-x 8)
                (< quad-y 1) (> quad-y 8))
        (progn
          (with-window-redisplay *output-window*
            (terpri *output-window*)
            (write-line "Not a valid quadrant." *output-window*)
            (terpri *output-window*))
          (return-from navigation nil)))

      (let 
          ((data (calculate-relative-course 
                  (sub-locations
                   (make-location 
                    :quad-x (1- quad-x) 
                    :quad-y (1- quad-y)
                    :sect-x (location-sect-x *lambda-location*)
                    :sect-y (location-sect-y *lambda-location*))
                   *lambda-location*))))
        (with-window-redisplay *output-window*
          (format *output-window* 
                  "~%Navigation~%Course = ~3,2F~%Distance= ~3,2F~2%"
                  (first data) (second data)))))))
	      
	      
(defun status-report ()
  (with-window-redisplay *output-window*
    (format *output-window* "~%Status report~%")
    (format *output-window* "There are ~D Cobolarg~P left.~%" 
	    *total-cobolargs* *total-cobolargs*)
    (format *output-window* "Mission must be completed in ~D day~P.~%"
	    *remaining-days* *remaining-days*)
    (format *output-window* 
	    "The federation is mentaining ~D starbase~P in the galaxy.~%"
	    *total-starbases* *total-starbases*)
    nil))

(defun galactic-region-name-map ()
  (display-file +map-file+ *output-window*))

(defun library-computer ()
  (block computer
    (when (> *library-computer-status* .85)
      (progn
	(with-window-redisplay *output-window*
	  (write-line "Library computer disabled." *output-window*))
	(return-from computer nil)))
    (let ((command 
	   (ignore-errors (number-query "Computer active and awaiting command? "))))
      (case command
	(0 (cumulative-galacic-record))
	(1 (status-report))
	(2 (photon-torpedo-data))
	(3 (starbase-nav-data))
	(4 (navigation-calculator))
	(5 (galactic-region-name-map))
	(otherwise
	 (with-window-redisplay *output-window*
	   (terpri *output-window*)
	   (write-line "The library computer accepts the following commands:" *output-window*)
	   (write-line "  0  -- Cumulative galactic record" *output-window*)
	   (write-line "  1  -- Status report" *output-window*)
	   (write-line "  2  -- Photon torpedo data" *output-window*)
	   (write-line "  3  -- Starbase navigation data" *output-window*)
	   (write-line "  4  -- navigation calculator" *output-window*)
	   (write-line "  5  -- Galactic region name map" *output-window*)
	   (terpri *output-window*))))
      nil)))

(defun select-command ()
  (let ((command 
         (progn 
           (setf (prompt-of *input-window*) "COMMAND? ")
           (ignore-errors (string-upcase (read-line *input-window*)))))
        (quit nil))
    (cond
      ((string= command "NAV") (warp-engine-control))
      ((string= command "SRS") (short-range-sensor-scan))
      ((string= command "LRS") (long-range-sensor-scan))
      ((string= command "PHA") (phaser-control))
      ((string= command "TOR") (photon-torpedo-control))
      ((string= command "SHE") (shield-control))
      ((string= command "DAM") (damage-control-report))
      ((string= command "COM") (library-computer))
      ((string= command "XXX") (setq quit t))
      (t 
       (with-window-redisplay *output-window*
         (write-line "NAV - navigate command" *output-window*)
         (write-line "SRS - short range sensor scan" *output-window*)
         (write-line "LRS - long range sensor scan" *output-window*)
         (write-line "PHA - phaser control" *output-window*)
         (write-line "TOR - photon torpedo control" *output-window*)
         (write-line "SHE - shield control" *output-window*)
         (write-line "DAM - damage control" *output-window*)
         (write-line "COM - library computer" *output-window*)
         (write-line "XXX - resign command" *output-window*))))
    quit))

(defun describe-settings ()
  (with-window-redisplay *output-window*
    (format *output-window* 
	    "~2%Your instructions are as follows:")
    (format *output-window* 
	    "~%     Destroy the ~D Cobolarg warships which have invaded"
	    *total-cobolargs*)
    (format *output-window*
           "~%  the galaxy before they can destroy the federation")
    (format *output-window* 
	    "~%  headquarters on stardate ~D.  This gives you ~D days."
	    *end-date* *remaining-days*)
    (format *output-window*
	    (concatenate 'string
	      "~%  There are ~D starbase~P in the galaxy for"
	      " resupplying your ship.~2%")
	    *total-starbases* *total-starbases*)
    (format *output-window*
	    "~2%Your mission begins with your starship located")
    (format *output-window*
	    "~%in the galactic quadrant '~A'.~2%"
	    (get-historical-name 
	     (1+ (location-quad-x *lambda-location*))
	     (1+ (location-quad-y *lambda-location*))))))

 (defun found-starbase-quadrant ()
   (let* ((quad-x (location-quad-x *lambda-location*))
 	 (quad-y (location-quad-y *lambda-location*))
 	 (sect-x (location-sect-x *lambda-location*))
 	 (sect-y (location-sect-y *lambda-location*))
 	 (starbase-list 
 	  (sector-starbase-list (aref *quadrants* quad-x quad-y))))

     (if (and starbase-list
 	    (find-if 
 	     #'(lambda (x) (and (= (first x)  sect-x) (= (second x) sect-y)))
 	     starbase-list))
 	t
       nil)))

(defun dock-to-starbase ()
  (with-window-redisplay *output-window*
    (write-line "Shields dropped for docking purposes." *output-window*))
    (setf *energy* +start-energy+)
    (setf *shield-energy* 0)
    (setf *photon-torpedoes* +start-photon-torpedoes+)
    (complete-repair-lambda-damage))


(defun setup-paramaters ()
  (setf *energy* +start-energy+)
  (setf *shield-energy* 0)
  (setf *lost-game* nil)
  (setf *won-game* nil)
  (complete-repair-lambda-damage))

(defun update-date (number)
  (decf *remaining-days* number)
  (when (<= *remaining-days* 0)
    (with-window-redisplay *output-window*
      (format *output-window* 
	      "~2%It is stardate ~D. Your time is up!~%"
	      *end-date*)
    (write-line 
     "The Cobolarg starships have now reached federation headquarters" 
     *output-window*)
    (write-line "and concured the federation." *output-window*)
    (write-line "The game is over." *output-window*)
    (format *output-window*
	    "~&At the end of the game there were ~D Cobolarg~P remaing.~2%"
	    *total-cobolargs* *total-cobolargs*))
    (setf *lost-game* t)))

(defun play-again-p ()
   (let ((command
	  (progn
	    (with-window-redisplay *output-window*
	      (write-line "The federation is in need of a new starship commander" *output-window*)
	      (write-line "for a simular mission -- If there is a voulenteer" *output-window*)
	      (write-string "let him step forward and enter 'AYE' ? " *output-window*))
	    (setf (prompt-of *input-window*) "AYE ? ")
	    (ignore-errors (string-upcase (read-line *input-window*))))))
     (if (and (stringp command) (string= command "AYE")) t nil)))




(defun lisptrek ()
  (block main
    (with-window-redisplay *title-window*
      (add-string-to-window *title-window* "Lisp Trek")
      (setf (background-of *title-window*) 'blue)
      (setf (foreground-of *title-window*) 'yellow))
    (with-window-redisplay *output-window*
      (write-line "Welcome to LispTrek the game." *output-window*)
      (terpri *output-window*))
    (when (confirm-p "Do you need instructions? ")
      (display-file +help-file+ *output-window*))
    (loop
       (setup-galaxy)
       (setup-lambda-location)
       (setup-paramaters)
       (display-file +pict-file+ *output-window*)
       (describe-settings)
       (short-range-sensor-scan)
     (block game
       (loop
	(let ((old-location *lambda-location*))
	  (when (select-command) 
	    (return-from game nil))
	
	  (unless (and
		   (= (location-quad-x *lambda-location*) 
		      (location-quad-x old-location))
		   (= (location-quad-y *lambda-location*)
		      (location-quad-y old-location)))
	    (with-window-redisplay *output-window*
	      (format *output-window*
		      "~2%Now entering '~A' quadrant.~2%"
		      (get-historical-name
		       (location-quad-x *lambda-location*)
		     (location-quad-y *lambda-location*))))
	    (partial-repair-lambda-damage)
	    (short-range-sensor-scan)
	    (update-date 1))
	  (if (found-starbase-quadrant)
	      (dock-to-starbase)
	    (when (found-cobolargs-quadrant)
	      (cobolarg-phaser-fire)))
	  (when (or *won-game* *lost-game*) (return-from game)))))
       (unless (play-again-p) (return-from main nil)))))






		       	      		



