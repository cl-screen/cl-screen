
(in-package :cl-user)

(eval-when (:execute :load-toplevel :compile-toplevel)
  (defpackage :cl-screen (:use :cl :cffi :trivial-gray-streams)
	      (:export
           :write-string-at-cursor :width-of :whitespacep :wait-key 
           :valid-key-p :valid-colors :valid-color-p :use-ansi-colors-p 
           :top-y-of :text-of :sltt-tgetstr :sltt-set-cursor-visibility 
           :sltt-set-color :sltt-get-terminfo :sltt-get-screen-size 
           :sltt-beep :slsmg-write-string :slsmg-write-char :slsmg-set-color 
           :slsmg-set-char-set :slsmg-reset-smg :slsmg-reinit-smg 
           :slsmg-refresh :slsmg-init-smg :slsmg-gotorc :slsmg-get-row 
           :slsmg-get-column :slsmg-erase-eos :slsmg-erase-eol 
           :slsmg-draw-vline :slsmg-draw-hline :slsmg-cls :slkp-init 
           :slkp-getkey :slkp-define-keysym :slang-unget-key :slang-reset-tty 
           :slang-process-keystring :slang-input-pending :slang-init-tty 
           :slang-getkey :slang-flush-input :set-to-default-color 
           :set-cursor-visibility :set-cursor :set-colors-from-attribute 
           :set-color :screen-resize-hook :screen-of :row-of :right-x-of 
           :resize-handler :remove-char-from-history-line :release-screen 
           :refresh-window :random-element :prompt-of 
           :print-history-line-to-window :previous-char-of 
           :position-window-cursor :parse-float :make-attribute 
           :lines-of :left-x-of :last-line-of :initialize-screen 
           :initialize-color-hash :height-of :goto-screen-rc 
           :get-screen-size :get-key :get-decoded-key :get-cursor 
           :get-color-pair-number :foreground-of :foreground-color-of 
           :finish-screen :erase-from-cursor-to-eos :erase-from-cursor-to-eol 
           :encode-key :draw-line-at-cursor :decode-key :cursor-y-of 
           :cursor-x-of :cursor-of :cursor-back :current-char-of :color-code 
           :code-color :clear-screen-resize-hook :clear-screen 
           :clear-line-in-window :bottom-y-of :background-of 
           :background-color-of :at-screen-rc :add-string-to-window 
           :add-space-to-history-line :add-line :add-char-to-window 
           :add-char-to-history-line :add-char 
           :*sl_key_up* :*sl_key_undo* :*sl_key_right* :*sl_key_redo* 
           :*sl_key_ppage* :*sl_key_npage* :*sl_key_left* :*sl_key_ic* 
           :*sl_key_home* :*sl_key_f9* :*sl_key_f8* :*sl_key_f7* 
           :*sl_key_f6* :*sl_key_f5* :*sl_key_f4* :*sl_key_f3* :*sl_key_f2* 
           :*sl_key_f1* :*sl_key_f0* :*sl_key_enter* :*sl_key_end* 
           :*sl_key_down* :*sl_key_delete* :*sl_key_c3* :*sl_key_c1* 
           :*sl_key_backspace* :*sl_key_b2* :*sl_key_a3* :*sl_key_a1* 
           :*screen-rows* :*screen-resize-hook* :*screen-cols* 
           :*color-names* :*color-hash* :*backgrounds*)))

(in-package :cl-screen)