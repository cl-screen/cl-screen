
(in-package :cl-screen)

(defvar *winch* 0)

(defvar *winch-hook* nil)

(defun sigwinch-handler (&rest ignored)
  (declare (ignore ignored))
  (sltt-get-screen-size)
  (slsmg-reinit-smg)
  (if *winch-hook*
      (funcall *winch-hook*)))


(sb-sys:enable-interrupt sb-unix:sigwinch #'sigwinch-handler)

